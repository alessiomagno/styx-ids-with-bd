package Utils

import java.time.Instant
import java.util.regex.Pattern

object AlienParser {
    
    def parseAlienRecord(input: String): String ={
        
        val classification = Pattern.compile("(?<=\\[Classification\\:\\s)(.*?)(?=\\])")
        val priority = Pattern.compile("(?<=\\[Priority\\:\\s)(.*?)(?=\\])")
    
        val matcherClass = classification.matcher(input)
    
        var inputClass = ""
        while(matcherClass.find()) {
            inputClass = input
                .replaceAll("\\[Classification\\:\\s(.*?)\\]", matcherClass.group(0))
                .replace("[**]", ",")
                //.replace("[", ",")
                //.replace("]", ",")
                .replace("{", ",")
                .replace("}", ",")
                .replace("->", ",")
                .replace("[P",",[P")
        
        }
    
        val matcherPrior = priority.matcher(inputClass)
        var res = ""
        while(matcherPrior.find()){
            res = inputClass
                .replaceAll("\\[Priority\\:\\s(.*?)\\]", matcherPrior.group(0))
                .replaceAll("\\[[1-9]\\:(.*?)\\]","")
                .replaceAll("[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}\\-[0-9]{2}\\:[0-9]{2}\\:[0-9]{2}\\.[0-9]{6}","")
                .replace(":",",")
                .filterNot((x: Char) => x.isWhitespace)
                .substring(1)
                .concat(","+Instant.now.getEpochSecond)
        }
        
        res
    }
}
