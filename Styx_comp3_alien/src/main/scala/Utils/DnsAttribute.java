package Utils;

public enum DnsAttribute {

    ID,RRNAME,RRTYPE,TX_ID, TYPE;

    @Override
    public String toString() {
        return super.name().toLowerCase();
    }
}
