package Utils;

public enum FlowAttribute {

    TIMESTAMP,
    DEST_IP,
    FLOW_ID,
    EVENT_TYPE,
    DNS,
    SRC_IP,
    VLAN,
    IN_IFACE,
    SRC_PORT,
    DEST_PORT,
    PROTO,
    NONE;

    @Override
    public String toString() {
        return super.name().toLowerCase();
    }
}
