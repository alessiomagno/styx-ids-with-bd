package utils;

import com.google.gson.Gson;


public class NetFlowParser {

    public static SessionUpdate parseRow(String input)  {

        return new Gson().fromJson(input,SessionUpdate.class);

    }
}

