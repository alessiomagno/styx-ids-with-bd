package utils;

public class SessionUpdate
{
    private String IPV4_SRC_ADDR;

    public String getIPV4SRCADDR() { return this.IPV4_SRC_ADDR; }

    public void setIPV4SRCADDR(String IPV4_SRC_ADDR) { this.IPV4_SRC_ADDR = IPV4_SRC_ADDR; }

    private String IPV4_DST_ADDR;

    public String getIPV4DSTADDR() { return this.IPV4_DST_ADDR; }

    public void setIPV4DSTADDR(String IPV4_DST_ADDR) { this.IPV4_DST_ADDR = IPV4_DST_ADDR; }

    private String IPV4_NEXT_HOP;

    public String getIPV4NEXTHOP() { return this.IPV4_NEXT_HOP; }

    public void setIPV4NEXTHOP(String IPV4_NEXT_HOP) { this.IPV4_NEXT_HOP = IPV4_NEXT_HOP; }

    private Double INPUT_SNMP;

    public Double getINPUTSNMP() { return this.INPUT_SNMP; }

    public void setINPUTSNMP(Double INPUT_SNMP) { this.INPUT_SNMP = INPUT_SNMP; }

    private Double OUTPUT_SNMP;

    public Double getOUTPUTSNMP() { return this.OUTPUT_SNMP; }

    public void setOUTPUTSNMP(Double OUTPUT_SNMP) { this.OUTPUT_SNMP = OUTPUT_SNMP; }

    private Double IN_PKTS;

    public Double getINPKTS() { return this.IN_PKTS; }

    public void setINPKTS(Double IN_PKTS) { this.IN_PKTS = IN_PKTS; }

    private Double IN_BYTES;

    public Double getINBYTES() { return this.IN_BYTES; }

    public void setINBYTES(Double IN_BYTES) { this.IN_BYTES = IN_BYTES; }

    private Double FIRST_SWITCHED;

    public Double getFIRSTSWITCHED() { return this.FIRST_SWITCHED; }

    public void setFIRSTSWITCHED(Double FIRST_SWITCHED) { this.FIRST_SWITCHED = FIRST_SWITCHED; }

    private Double LAST_SWITCHED;

    public Double getLASTSWITCHED() { return this.LAST_SWITCHED; }

    public void setLASTSWITCHED(Double LAST_SWITCHED) { this.LAST_SWITCHED = LAST_SWITCHED; }

    private Double L4_SRC_PORT;

    public Double getL4SRCPORT() { return this.L4_SRC_PORT; }

    public void setL4SRCPORT(Double L4_SRC_PORT) { this.L4_SRC_PORT = L4_SRC_PORT; }

    private Double L4_DST_PORT;

    public Double getL4DSTPORT() { return this.L4_DST_PORT; }

    public void setL4DSTPORT(Double L4_DST_PORT) { this.L4_DST_PORT = L4_DST_PORT; }

    private Double TCP_FLAGS;

    public Double getTCPFLAGS() { return this.TCP_FLAGS; }

    public void setTCPFLAGS(Double TCP_FLAGS) { this.TCP_FLAGS = TCP_FLAGS; }

    private Double PROTOCOL;

    public Double getPROTOCOL() { return this.PROTOCOL; }

    public void setPROTOCOL(Double PROTOCOL) { this.PROTOCOL = PROTOCOL; }

    private Double SRC_TOS;

    public Double getSRCTOS() { return this.SRC_TOS; }

    public void setSRCTOS(Double SRC_TOS) { this.SRC_TOS = SRC_TOS; }

    private Double SRC_AS;

    public Double getSRCAS() { return this.SRC_AS; }

    public void setSRCAS(Double SRC_AS) { this.SRC_AS = SRC_AS; }

    private Double DST_AS;

    public Double getDSTAS() { return this.DST_AS; }

    public void setDSTAS(Double DST_AS) { this.DST_AS = DST_AS; }

    private Double IPV4_SRC_MASK;

    public Double getIPV4SRCMASK() { return this.IPV4_SRC_MASK; }

    public void setIPV4SRCMASK(Double IPV4_SRC_MASK) { this.IPV4_SRC_MASK = IPV4_SRC_MASK; }

    private Double IPV4_DST_MASK;

    public Double getIPV4DSTMASK() { return this.IPV4_DST_MASK; }

    public void setIPV4DSTMASK(Double IPV4_DST_MASK) { this.IPV4_DST_MASK = IPV4_DST_MASK; }

    private long TOTAL_FLOWS_EXP;

    public long getTOTALFLOWSEXP() { return this.TOTAL_FLOWS_EXP; }

    public void setTOTALFLOWSEXP(long TOTAL_FLOWS_EXP) { this.TOTAL_FLOWS_EXP = TOTAL_FLOWS_EXP; }
}