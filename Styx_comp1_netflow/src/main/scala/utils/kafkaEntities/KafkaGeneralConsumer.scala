package kafkaEntities

import java.util
import java.util.Properties

import kafka.consumer.{Consumer, ConsumerConfig}

import scala.collection.JavaConversions._

object KafkaGeneralConsumer {
    def main(args: Array[String]): Unit = {
        val simpleHLConsumer = new KafkaGeneralConsumer("137.204.72.74:2181", "testgroup", "test")
        simpleHLConsumer.testConsumer()
    }
}

class KafkaGeneralConsumer(val zookeeper: String, val groupId: String, val topic: String) {
    val props = new Properties
    props.put("zookeeper.connect", zookeeper)
    props.put("group.id", groupId)
    props.put("zookeeper.session.timeout.ms", "500")
    props.put("zookeeper.sync.time.ms", "250")
    props.put("auto.commit.interval.ms", "1000")
    final private val consumer = Consumer.createJavaConsumerConnector(new ConsumerConfig(props))
    
    def testConsumer(): Unit = {
        val topicCount = new util.HashMap[String, Integer]
        topicCount.put(topic, 1)
        val consumerStreams = consumer.createMessageStreams(topicCount)
        val streams = consumerStreams.get(topic)
        for (stream <- streams) {
            val it = stream.iterator
            //while(it.hasNext) System.out.println(FlowParser.getValueFromKey(new String(it.next.message),FlowAttribute.FLOW_ID))
        }
        if (consumer != null) consumer.shutdown()
    }
}