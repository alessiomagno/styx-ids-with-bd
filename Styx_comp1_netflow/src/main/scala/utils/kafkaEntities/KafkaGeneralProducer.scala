package kafkaEntities

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

class KafkaGeneralProducer() {
    
    final private val properties = new Properties
    properties.put("bootstrap.servers", "137.204.72.84:9092")
    properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    
   
    val producer = new KafkaProducer[String, String](properties)
    
    def publish(record: String, topic: String): Unit = {
        val data = new ProducerRecord[String, String](topic, record)
        producer.send(data)
    }
    
    def close(): Unit = producer.close()
    
}