package Utils;

public class FlowUpdate {

    private String timestamp;

    private String dest_ip;

    private String flow_id;

    private String event_type;

    private DnsDetail dns;

    private String src_ip;

    private String vlan;

    private String in_iface;

    private String src_port;

    private String dest_port;

    private String proto;

    public String gettimestamp()
    {
        return timestamp;
    }

    public void settimestamp (String timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getdest_ip ()
    {
        return dest_ip;
    }

    public void setfest_ip (String dest_ip)
    {
        this.dest_ip = dest_ip;
    }

    public String getflow_id ()
    {
        return flow_id;
    }

    public void setflow_id (String flow_id)
    {
        this.flow_id = flow_id;
    }

    public String getevent_type ()
    {
        return event_type;
    }

    public void setevent_type (String event_type)
    {
        this.event_type = event_type;
    }

    public DnsDetail getdns () { return dns; }

    public void setdns (DnsDetail dns)
    {
        this.dns = dns;
    }

    public String getsrc_ip ()
    {
        return src_ip;
    }

    public void setsrc_ip (String src_ip)
    {
        this.src_ip = src_ip;
    }

    public String getvlan ()
    {
        return vlan;
    }

    public void setvlan (String vlan)
    {
        this.vlan = vlan;
    }

    public String getin_iface ()
    {
        return in_iface;
    }

    public void setIn_iface (String in_iface)
    {
        this.in_iface = in_iface;
    }

    public String getsrc_port ()
    {
        return src_port;
    }

    public void setsrc_port (String src_port)
    {
        this.src_port = src_port;
    }

    public String getdest_port ()
    {
        return dest_port;
    }

    public void setdest_port (String dest_port)
    {
        this.dest_port = dest_port;
    }

    public String getproto ()
    {
        return proto;
    }

    public void setproto (String proto)
    {
        this.proto = proto;
    }

    @Override
    public String toString()
    {
        return "FlowUpdate [timestamp = "+timestamp+", dest_ip = "+dest_ip+", flow_id = "+flow_id+", event_type = "+event_type+", dns = "+dns+", src_ip = "+src_ip+", vlan = "+vlan+", in_iface = "+in_iface+", src_port = "+src_port+", dest_port = "+dest_port+", proto = "+proto+"]";
    }
}
