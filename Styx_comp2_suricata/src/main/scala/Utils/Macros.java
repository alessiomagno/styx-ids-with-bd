package Utils;

public class Macros {

    public static final int sourcePortNetFlow= 8000;
    public static final int sourcePortSuricata= 8100;
    public static final int sourcePortAlien= 8200;

    public static final String topicComp1 = "netFlow";
    public static final String topicComp2 = "suricata";
    public static final String topicComp3 = "alien";
    public static final String zookeeperAdr = "137.204.72.74";
    public static final int zookeeperPort = 2181;
    public static final String brokerAdr = "137.204.72.84";

}
