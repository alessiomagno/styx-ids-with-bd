package Utils;

import com.google.gson.Gson;


public class SuricataParser {

    public static FlowUpdate parseRow(String input)  {

        return new Gson().fromJson(input,FlowUpdate.class);

    }
}

