package Utils;

public class DnsDetail {

    private String id;

    private String rrname;

    private String rrtype;

    private String tx_id;

    private String type;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getRrname ()
    {
        return rrname;
    }

    public void setRrname (String rrname)
    {
        this.rrname = rrname;
    }

    public String getRrtype ()
    {
        return rrtype;
    }

    public void setRrtype (String rrtype)
    {
        this.rrtype = rrtype;
    }

    public String getTx_id ()
    {
        return tx_id;
    }

    public void setTx_id (String tx_id)
    {
        this.tx_id = tx_id;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "DnsDetail [id = "+id+", rrname = "+rrname+", rrtype = "+rrtype+", tx_id = "+tx_id+", type = "+type+"]";
    }
}