import java.io._
import java.net._
import java.time.Instant

import Utils.{Macros, SuricataParser}


//import kafkaEntities.KafkaGeneralProducer

object SuricataWrapper extends App{
    
    val server = new ServerSocket(Macros.sourcePortSuricata)
    println("-Start Suricata Catcher-")
    val s = server.accept()
    //val test = new KafkaGeneralProducer
    println("-Socket opened-")
    val in = new BufferedReader(new InputStreamReader(s.getInputStream()))
    var input = in readLine()
    val pw = new PrintWriter(new File("dataset_source2_netflow_1307.txt" ))
    while (input != null) {
        val flowUpdate = SuricataParser.parseRow(input)
         /*   if(flowUpdate.getdns()!= null){
            pw.write(Instant.now.getEpochSecond+","+
                flowUpdate.getflow_id()+","+
                flowUpdate.getsrc_ip()+","+
                flowUpdate.getsrc_port()+","+
                flowUpdate.getdest_ip()+","+
                flowUpdate.getdest_port()+","+
                flowUpdate.getproto()+","+
                flowUpdate.getevent_type()+","+
                flowUpdate.getvlan()+","+
                flowUpdate.getin_iface()+","+
                flowUpdate.getdns().getId+","+
                flowUpdate.getdns().getRrname+","+
                flowUpdate.getdns().getRrtype+","+
                flowUpdate.getdns().getType+
                "\n")
            }
        else {*/
                pw.write(Instant.now().getEpochSecond + "," +
                    flowUpdate.getflow_id() + "," +
                    flowUpdate.getsrc_ip() + "," +
                    flowUpdate.getsrc_port() + "," +
                    flowUpdate.getdest_ip() + "," +
                    flowUpdate.getdest_port() + "," +
                    flowUpdate.getproto() + "," +
                    flowUpdate.getevent_type() + "," +
                    flowUpdate.getvlan() + "," +
                    flowUpdate.getin_iface() +
                "\n")
            
            input = in readLine()
        }
        pw.close
    s.close()
    
}