# Styx - Progetto di Tesi Magistrale "Ingegneria e Scienze Informatiche"


Negli ultimi anni, nel panorama digitale, è stato rilevato un ingente aumento del numero di dispositivi e utenti con accesso ad Internet.
 Proporzionalmente a questi fattori ogni giorno vengono generati continuamente, e in qualsiasi contesto, grandi quantità di dati difficili da gestire. Questo ha fatto emergere la necessità di riorganizzare gli asset aziendali per far fronte ad un calibro di informazione maggiore e per far in modo che la gestione stessa ne estragga valore concreto per la realtà decisionale.
 L'insieme di queste motivazioni da vita al fenomeno dei Big Data. 
 Affiancato a questo panorama, inoltre, la grande quantità macchine e utenti in rete ha esponenzialmente aumentato anche il numero di attacchi informatici, che puntano nella stragrande dei casi all'appropriazione non autorizzata di dati sensibili e/o a provocare disservizi nelle reti private.
 Un esempio è il campus universitario di Forlì-Cesena che stima costantemente attive circa 3000 macchine interconnesse tra di loro e con la rete esterna.
 La grande quantità di risorse connesse in rete assume una certa importanza visti i dati sensibili che gestiscono e immagazzinano e nonostante l'archittettura di monitoraggio venga continuamente aggiornata, quest'ultima presenta colli di bottiglia evidenti e limitazioni nell'elaborazione dell'intero traffico di rete.
 Per far fronte a questa problematica lo scopo della tesi è stato quello di far convergere questi due ambiti informatici integrando al processo di sicurezza della rete un sistema di analisi e monitoraggio per il rilevamento di intrusioni (intrusion detection system), su piattaforma Big Data.
 Il prototipo realizzato (denominato Styx), sfrutta tecniche di data stream processing (elaborazione di dati real-time) e di machine learning (tecniche di apprendimento per estrazione di modelli predittivi) per potenziare l'attuale sistema di monitoraggio della rete universitaria.