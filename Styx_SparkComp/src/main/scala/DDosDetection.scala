import org.apache.spark.sql.{DataFrame, SparkSession}

class DDosDetection(netflowSessDF: DataFrame) {
    
    def analyzeSessions: DataFrame = {
        
        netflowSessDF.createOrReplaceTempView("sessionlayer1")
        val spark = SparkSession.builder
            .master("local")
            .getOrCreate
        spark.sqlContext.sql("select ip1,ip2,src_port,count(*) as count,sum(duration) as total_duration from sessionlayer group by ip1,ip2,src_port")
    }
    
}
