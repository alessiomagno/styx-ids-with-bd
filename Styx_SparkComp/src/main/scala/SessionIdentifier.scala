import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat
import org.apache.spark
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.feature.StringIndexer
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.mllib.tree.configuration.Strategy
import org.apache.spark.sql.{DataFrame, Row, SaveMode, SparkSession}
import org.apache.spark.sql.types._
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext, rdd}
import org.apache.spark.sql.functions.expr
import org.apache.spark.sql.functions._


object SessionIdentifier {
  
    
  def main(args: Array[String]) {
      val conf = new SparkConf().setAppName("Styx - IDS with BigData")
      //.setMaster("local[*]")
      val sc = new SparkContext(conf)
      println("Spark application start - session identifier module")
    
      val spark = SparkSession.builder
          .config("spark.driver.maxResultSize", "2g")
          .getOrCreate
    
      val sqlContext = spark.sqlContext
      sqlContext.sql("SET spark.sql.autoBroadcastJoinThreshold = -1")
      
      val df = spark.read.csv("src1_test.csv")
      df.createOrReplaceTempView("netflow")
      //ESTIMATION PART volume
      //val estimationVolumePerSecond = sqlContext.sql("select netflow._c4,sum(netflow._c7) from netflow group by netflow._c4").coalesce(1).write.csv("hdfs:///user/aaddimando/results")
      //val sessionsPerRangeOfByte = sqlContext.sql("select sum(netflow._c7) from netflow group by netflow._c0,netflow._c1,netflow._c2").coalesce(1).write.csv("hdfs:///user/aaddimando/results")
    
      val session1 = sqlContext.sql("select netflow._c0 as ip1, netflow._c2 as ip2 , netflow._c1 as src_ports, netflow._c3 as dst_ports, netflow._c4 as start, MAX(netflow._c5) as end, sum(netflow._c7) as byteUP from netflow group by netflow._c0,netflow._c1,netflow._c2,netflow._c3,netflow._c4")
      val session2 = sqlContext.sql("select netflow._c2 as ip1, netflow._c0 as ip2, netflow._c3 as src_ports, netflow._c1 as dst_ports, netflow._c4 as start,  MAX(netflow._c5) as end ,sum(netflow._c7) as byteDOWN from netflow group by netflow._c2,netflow._c0,netflow._c3,netflow._c1,netflow._c4")
    
      val myExpression = "end - start"
      val sessionjoin = session1.join(
          session2, Seq("ip1", "src_ports", "ip2", "dst_ports", "start", "end")).withColumn("duration", expr(myExpression))
      session1("ip1") === session2("ip1")
                  && session1("ip2") === session2("ip2")
                  && session1("src_ports") === session2("src_ports")
                  && session1("dst_ports") === session2("dst_ports")
                  && session1("start") === session2("start")
              , "full_outer")
    
      //sessionjoin.show(10)
    
      val netflowSessions = sessionjoin.rdd
          .map({ x =>
              if (x(0).toString.startsWith("137.204") || x(0).toString.startsWith("10.")) Row(x(0), x(1), x(2), x(3), x(4), x(5), x(6), x(7), x(8))
              else Row(x(2), x(3), x(0), x(1), x(4), x(5), x(6), x(7), x(8))
          })
    
    
      val netflowSesSchema = List(
          StructField("ip1", StringType, true),
          StructField("src_port", StringType, true),
          StructField("ip2", StringType, true),
          StructField("dst_port", StringType, true),
          StructField("start", StringType, true),
          StructField("end", StringType, true),
          StructField("byteIN", DoubleType, true),
          StructField("byteOUT", DoubleType, true),
          StructField("duration", DoubleType, true)
      )
    
      val netflowSessDF = spark.createDataFrame(
          netflowSessions,
          StructType(netflowSesSchema)
      )
          .withColumn("srcPort", col("src_port").cast(IntegerType)).drop("src_port").withColumnRenamed("srcPort", "src_port")
          .withColumn("dstPort", col("dst_port").cast(IntegerType)).drop("dst_port").withColumnRenamed("dstPort", "dst_port")
          .withColumn("startTime", col("start").cast(IntegerType)).drop("start").withColumnRenamed("startTime", "timestamp")
          .withColumn("endTime", col("end").cast(IntegerType)).drop("end").withColumnRenamed("endTime", "end")
          .distinct()
    
      //ESTIMATION PART duration
      // netflowSessDF.createOrReplaceTempView("netflow")
      //val sessionsPerDuration = sqlContext.sql("select sum(duration) from netflow group by ip1,ip2,dst_port").coalesce(1).write.csv("hdfs:///user/aaddimando/result")
      //sqlContext.sql("select ip1,count(*),sum(byteIN) as byte ,sum(duration) as time from netflow group by ip1").filter(col("byte") > 1000000000 && col("time") < 18000).coalesce(1).write.csv("hdfs:///user/aaddimando/result1")
    
    
      val portscanDetection = new PortScanDetection(netflowSessDF).analyzeSessions
       portscanDetection.show(10)
      netflowSessDF.createOrReplaceTempView("sessionlayer")
      spark.sqlContext.sql("select ip1,ip2,src_port,count(*) as count,sum(duration) as total_duration from sessionlayer group by ip1,ip2,src_port").rdd.repartition(1).saveAsTextFile("hdfs:///user/aaddimando/PortscanDetection.csv")
      spark.sqlContext.sql("select ip1,count(*) as count from sessionlayer group by ip1").rdd.repartition(1).saveAsTextFile("hdfs:///user/aaddimando/DDS.csv")
    
    
      val df2 = spark.read.csv("src2_test.csv")
          .drop("_c10")
          .withColumnRenamed("_c0", "timestamp2")
          .withColumnRenamed("_c1", "id")
          .withColumnRenamed("_c2", "ip1")
          .withColumnRenamed("_c3", "src_port")
          .withColumnRenamed("_c4", "ip2")
          .withColumnRenamed("_c5", "dst_port")
          .withColumnRenamed("_c6", "proto")
          .withColumnRenamed("_c7", "event_type")
          .withColumnRenamed("_c8", "vlan")
          .withColumnRenamed("_c9", "in_face")
    
       val suriAlert = df2
                  .rdd
                  .map { x =>
                      if (x(2).toString.startsWith("137.204.") && x(4).toString.startsWith("10.")) Row(x)
                      else if (x(2).toString.startsWith("10.") && x(4).toString.startsWith("137.204.")) Row(x(0), x(1), x(4), x(5), x(2), x(3), x(6), x(7), x(8) , x(9))
                      else if (x(2).toString.startsWith("10.") || x(2).toString.startsWith("137.204.")) Row(x)
                      else Row(x(0), x(1), x(4), x(5), x(2), x(3), x(6), x(7), x(8), x(9))
                  }
              
              
    
      val suriAlert = df2
          .rdd
          .map { x =>
              if (x(2).toString.startsWith("137.204.") && x(4).toString.startsWith("10.")) Row(x(0), x(1), x(2), x(3), x(4), x(5), x(6), x(7), x(8), x(9))
              else if (x(2).toString.startsWith("10.") && x(4).toString.startsWith("137.204.")) Row(x(0), x(1), x(4), x(5), x(2), x(3), x(6), x(7), x(8), x(9))
              else if (x(2).toString.startsWith("10.") || x(2).toString.startsWith("137.204.")) Row(x(0), x(1), x(2), x(3), x(4), x(5), x(6), x(7), x(8), x(9))
              else Row(x(0), x(1), x(4), x(5), x(2), x(3), x(6), x(7), x(8), x(9))
            
          }
    
    
      val suriAlertSchema = List(
          StructField("timestamp3", StringType, true),
          StructField("id", StringType, true),
          StructField("ip1", StringType, true),
          StructField("src_port", StringType, true),
          StructField("ip2", StringType, true),
          StructField("dst_port", StringType, true),
          StructField("proto", StringType, true),
          StructField("event_type", StringType, true),
          StructField("vlan", StringType, true),
          StructField("in_face", StringType, true)
      )
    
      val suriAlertDF = spark.createDataFrame(
          suriAlert, StructType(suriAlertSchema)
      )
    
      suriAlertDF.show(10)
      val netSuriDF = suriAlertDF.join(netflowSessDF, Seq("ip1", "src_port", "ip2", "dst_port"), "full_outer")
          /* suriAlertDF("ip1") <=> netflowSessDF("ip1") &&
                      suriAlertDF("src_port") <=> netflowSessDF("src_port") &&
                      suriAlertDF("ip2") <=> netflowSessDF("ip2") &&
                      suriAlertDF("dst_port") <=> netflowSessDF("dst_port") &&
                      suriAlertDF("timestamp") <=> netflowSessDF("timestamp")
                  , "full_outer")*/
          .drop("end")
    
    
      val df3 = spark.read.csv("src3_test.csv")
          .withColumnRenamed("_c0", "classification")
          .withColumnRenamed("_c1", "priority")
          .withColumnRenamed("_c2", "proto3")
          .withColumnRenamed("_c3", "ip1")
          .withColumnRenamed("_c4", "src_port")
          .withColumnRenamed("_c5", "ip2")
          .withColumnRenamed("_c6", "dst_port")
          .withColumnRenamed("_c7", "timestamp4")
    
    
      val alienSchema = List(
          StructField("ip1", StringType, true),
          StructField("ip2", StringType, true),
          StructField("src_port", StringType, true),
          StructField("dst_port", StringType, true),
          StructField("description", StringType, true),
          StructField("classification", StringType, true),
          StructField("priority", StringType, true)
    
      )
    
    
      val alienLayer = df3
          .rdd
          .map { x => Row(x(4), x(6), x(5), x(7), x(0), x(1), x(2), x(3))
            
          }
    
      val alienDF = spark.createDataFrame(alienLayer, StructType(alienSchema))
    
      //netSuriDF.show(1000)
    
      //alienDF.show(1000)
    
      val level23 = alienDF.join(netSuriDF,
          Seq("ip1", "ip2", "src_port", "dst_port"), "full_outer")
    
      level23.show(10)
    
    
      val df4 = spark.read.csv("src4_test.csv")
      val roleTable = sc.parallelize(df4.collect().distinct)
    
    
      //roleTable.foreach(println)
    
      val roleSchema = List(
          StructField("ip", StringType, true),
          StructField("hostname", StringType, true),
          StructField("FQDNs", StringType, true),
          StructField("Asset_value", StringType, true),
          StructField("Operating System", StringType, true),
          StructField("Host ID", StringType, true)
    
      )
    
      val roleDF = spark.createDataFrame(roleTable, StructType(roleSchema))
      val patternIp = "[0-9]+\\.[0-9]+\\."
      val finalDataset = level23.join(roleDF,
          level23("ip1") === roleDF("ip")
          , "full_outer")
          .drop("ip")
          .drop("timestamp1")
          .drop("timestamp4")
          .drop("timestamp3")
          .drop("proto3")
          .drop("description")
          .drop("FQDNs")
          .drop("Host ID")
          .withColumnRenamed("timestamp2", "timestamp")
          .withColumn("type", when(!col("classification").isNull, "P").otherwise("N"))
          .withColumn("hostname", when(col("hostname").isNull && col("Asset_value").isNull, "standard-host"))
          .withColumn("Asset_value", when(col("hostname") === "standard-host", "1"))
          .withColumn("ip2Localized", regexp_extract(col("ip2"), patternIp, 0))
    
    
      val ipDatabase1 = spark.read.format("csv").option("header", "true").load("GeoLite2-Country.csv")
      val ipDatabase2 = spark.read.format("csv").option("header", "true").load("GeoLite2_locationid.csv")
      val ipDatasetFinal = ipDatabase1.join(ipDatabase2, Seq("geoname_id"), "left_outer").withColumn("ip2Localized", regexp_extract(col("network"), patternIp, 0))
          .drop("id", "geoname_id", "network", "registered_country_geoname_id", "country_iso_code")
          .drop("represented_country_geoname_id", "is_anonymous_proxy", "is_satellite_provider", "locale_code", "continent_code")
          .withColumnRenamed("is_in_european_union", "EU_country")
    
    
      val localizedDataset = finalDataset.join(broadcast(ipDatasetFinal), Seq("ip2Localized")).drop("ip2Localized")
    
      localizedDataset.createOrReplaceTempView("localDataset")
      sqlContext.sql("select country_name,count(*) where type = \"P\" from localDataset group by country_name").show(100)
  
      //sqlContext.sql("select count(*) where type = \"P\" from localDataset group by Volume").show(100)
      
      
      //ML PART
      val LDcolumnCateg = Array("classification", "event_type", "proto", "hostname", "in_face", "Operating System", "type", "continent_name", "country_name")
      val LDcolumnNumeric = Array("src_port", "dst_port", "priority", "vlan", "byteIN", "byteOUT", "duration", "timestamp", "Asset_value", "EU_country")
      val NormalizedDataset = localizedDataset.drop("ip1", "ip2", "id").na.fill("None", LDcolumnCateg).na.fill("0", LDcolumnNumeric)
    
      val indexers = LDcolumnCateg.map { colName =>
          new StringIndexer().setInputCol(colName).setOutputCol(colName + "_indexed")
              .setHandleInvalid("skip")
      }
    
      val pipeline = new Pipeline().setStages(indexers)
      val indexedDataset = pipeline.fit(NormalizedDataset).transform(NormalizedDataset)
    
      val MLDataset = indexedDataset
          .withColumn("Volume", when(col("byteIN") + col("byteOUT") > 10000000, "1").otherwise("0"))
          .withColumn("Duration", when(col("duration") < 1800, "0").otherwise("1"))
          .withColumn("srcPort_type", when(col("src_port") < 1024, "1").otherwise("0"))
          .withColumn("dstPort_type", when(col("dst_port") < 1024, "1").otherwise("0"))
          .select(
              "type_indexed",
              "srcPort_type",
              "dstPort_type",
              "Volume",
              "Duration",
              "vlan",
              "in_face_indexed",
              "Operating System_indexed",
              "Asset_value",
              "EU_country",
              "continent_name_indexed",
              "country_name_indexed",
              "timestamp",
              "proto_indexed",
              "hostname_indexed",
              "classification_indexed"
          ).na.drop
      
      
     val data = MLDataset
          .map(_.toString)
          .map{ x => x.replace("[", "") }
          .map { x => x.replace("]", "") }
          .filter(x => !x.contains("null")).rdd
          .coalesce(1).saveAsTextFile("hdfs:///user/aaddimando/MLDatasetStyx4")
  }
  
    
  
      //ML DECISION TREE PART 1
    
      import org.apache.spark.mllib.tree.DecisionTree
      import org.apache.spark.mllib.regression.LabeledPoint
      import org.apache.spark.mllib.linalg.Vectors
      import org.apache.spark.mllib.tree.configuration.Algo._
      import org.apache.spark.mllib.tree.impurity.Gini
    
    
      // Load and parse the data file
      val data = sc.textFile("MLDatasetStyx2/part-00000")
          .map { x => x.replace("[", "") }
          .map { x => x.replace("]", "") }
          .filter(x => !x.contains("null"))
    
      val r = new scala.util.Random
      val parsedData = data.map { line =>
          val parts = line.split(',').map(_.toDouble)
          if (r.nextInt(15) > 1) LabeledPoint(9999, Vectors.dense(parts.tail))
          else LabeledPoint(6666, Vectors.dense(parts.tail))
      }.take(100000)
    
      sc.parallelize(parsedData).coalesce(1).saveAsTextFile("hdfs:///user/aaddimando/MLDatasetStyxResamp2")
    
    
  

/*
  // Run training algorithm to build the model
  val maxDepth = 10
  val model = DecisionTree.train(parsedData, Classification, Gini, maxDepth)

  // Evaluate model on training examples and compute training error
  val labelAndPreds = parsedData.map { point =>
      val prediction = model.predict(point.features)
      (point.label, prediction)
  }
  val trainErr = labelAndPreds.filter(r => r._1 != r._2).count.toDouble / parsedData.count
  println("Test Error = " + trainErr)
  println("Learned classification tree model:\n" + model.toDebugString)
}}
*/

/*
  import org.apache.spark.mllib.linalg.Vectors
  import org.apache.spark.mllib.regression.LabeledPoint

      //ML RANDOM FOREST PART 2
      val data = sc.textFile("hdfs:///user/aaddimando/MLDatasetStyxResampl1/part-00000")
          .map { x => x.replace("[", "") }
          .map { x => x.replace("]", "") }
          .filter(x => !x.contains("null"))
    
      val parsedData = data.map { line =>
          val parts = line.split(',').map(_.toDouble)
          LabeledPoint(parts(0), Vectors.dense(parts.tail))
      }
    
      // Split data into training/test sets
    
      val splits = parsedData.randomSplit(Array(0.7, 0.3))
    
      val (trainingData, testData) = (splits(0), splits(1))
    
      val treeStrategy = Strategy.defaultStrategy("Classification")
    
      val numTrees = 65 // Use more in practice.
    
      val featureSubsetStrategy = "auto" // Let the algorithm choose.
    
      val model = RandomForest.trainClassifier(trainingData,
        
          treeStrategy, numTrees, featureSubsetStrategy, seed = 12345)
      // Evaluate model on test instances and compute test error
    
      val testErr = testData.map { point =>
        
          val prediction = model.predict(point.features)
        
          if (point.label == prediction) 1.0 else 0.0
        
      }.mean()
    
      val predictionAndLabels = testData.map { case LabeledPoint(label, features) =>
          val prediction = model.predict(features)
          (prediction, label)
      }
    
      val metrics = new BinaryClassificationMetrics(predictionAndLabels)
    
      // Precision by threshold
      val precision = metrics.precisionByThreshold
      precision.foreach { case (t, p) =>
          println(s"Threshold: $t, Precision: $p")
      }
    
      // Recall by threshold
      val recall = metrics.recallByThreshold
      recall.foreach { case (t, r) =>
          println(s"Threshold: $t, Recall: $r")
      }
    
      // Precision-Recall Curve
      val PRC = metrics.pr
    
      println("Test Error = " + testErr)
    
      //println("Learned Random Forest: " + model.toDebugString)
  }}
*/
  //ML CLUSTERING K-MEANS PART 3//
  /*
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.evaluation.ClusteringEvaluator

// Loads data.
val dataset = spark.read.format("csv").load("MLDatasetStyx/part-00000")

// Trains a k-means model.
val kmeans = new KMeans().setK(2).setSeed(1L)
val model = kmeans.fit(dataset)

// Make predictions
val predictions = model.transform(dataset)

// Evaluate clustering by computing Silhouette score
val evaluator = new ClusteringEvaluator()

val silhouette = evaluator.evaluate(predictions)
println(s"Silhouette with squared euclidean distance = $silhouette")

// Shows the result.
println("Cluster Centers: ")
model.clusterCenters.foreach(println)

*/



//STREAMING PART FOR PORTSCANNING AND DDOS
 
  val ssc = new StreamingContext(sc, Seconds(1))
      import sqlContext.implicits._
      import org.apache.spark.sql.functions._
      val topicMap = "test".split(",").map((_, 1)).toMap
      val lines = KafkaUtils.createStream(ssc, "137.204.72.74:2181", "group1", topicMap)
      var globalRDD = Seq((0,0,0,0,0,0,0,0,0),(0,0,0,0,0,0,0,0,0)).toDF("ip1","ip2","byteIN","byteOUT","duration","src_port","dst_port","timestamp","end")
    
      lines.foreachRDD(rdd =>
     if(!rdd.isEmpty()) {
         rdd.map(_._2).map(record => record.split(",")).map(r => (r(0), r(1), r(2), r(3), r(4), r(5), r(6), r(7), r(8))).toDF("a", "b", "c", "d", "e", "f", "g", "h", "i").createOrReplaceTempView("netflow")
         val session1 = sqlContext.sql("select a as ip1, c as ip2 , b as src_ports, d as dst_ports, e as start, MAX(f) as end, sum(h) as byteUP from netflow group by a,b,c,d,e")
         val session2 = sqlContext.sql("select c as ip1, a as ip2, d as src_ports, b as dst_ports, e as start,  MAX(f) as end ,sum(h) as byteDOWN from netflow group by c,a,d,b,e")
         val myExpression = "end - start"
         val sessionjoin = session1.join(session2, Seq("ip1", "src_ports", "ip2", "dst_ports", "start", "end")).withColumn("duration",expr(myExpression))
         val netflowSessions = sessionjoin.rdd
             .map({ x =>
                 if (x(0).toString.startsWith("137.204") || x(0).toString.startsWith("10.")) Row(x(0), x(1), x(2), x(3), x(4), x(5), x(6), x(7), x(8))
                 else Row(x(2), x(3), x(0), x(1), x(4), x(5), x(6), x(7), x(8))
             }) //.saveAsTextFile("hdfs:///user/aaddimando/try/quizas")
    
    
         val netflowSesSchema = List(
             StructField("ip1", StringType, true),
             StructField("src_port", StringType, true),
             StructField("ip2", StringType, true),
             StructField("dst_port", StringType, true),
             StructField("start", StringType, true),
             StructField("end", StringType, true),
             StructField("byteIN", DoubleType, true),
             StructField("byteOUT", DoubleType, true),
             StructField("duration", DoubleType, true)
         )
    
         val netflowSessDF = spark.createDataFrame(
             netflowSessions,
             StructType(netflowSesSchema)
         )
             .withColumn("srcPort", col("src_port").cast(IntegerType)).drop("src_port").withColumnRenamed("srcPort", "src_port")
             .withColumn("dstPort", col("dst_port").cast(IntegerType)).drop("dst_port").withColumnRenamed("dstPort", "dst_port")
             .withColumn("startTime", col("start").cast(IntegerType)).drop("start").withColumnRenamed("startTime", "timestamp")
             .withColumn("endTime", col("end").cast(IntegerType)).drop("end").withColumnRenamed("endTime", "end")
    
         globalRDD = globalRDD.join(netflowSessDF,Seq("ip1","ip2","byteIN","byteOUT","duration","src_port","dst_port","timestamp","end"),"inner")
         //globalRDD = globalRDD.union(netflowSessDF)
         globalRDD
             .coalesce(1)
             .write
             .format("com.databricks.spark.csv")
             .mode(SaveMode.Overwrite)
             .save("hdfs:///user/aaddimando/prova")
         
         globalRDD.createOrReplaceTempView("sessionlayer")
         //PORTSCAN
         sqlContext.sql("select ip1,ip2,src_port,count(*) as count,sum(duration) as total_duration from sessionlayer group by ip1,ip2,src_port").rdd.repartition(1).saveAsTextFile("hdfs:///user/aaddimando/PortscanDetectionR.csv")
         //DDOS
         sqlContext.sql("select ip1,count(*) where sum(byteIN) > 1000000000 AND sum(duration) < 18000 from sessionlayer1 group by ip1").rdd.repartition(1).saveAsTextFile("hdfs:///user/aaddimando/DDOSDetectionR.csv")
    
         
     })
      
      ssc.start()
      ssc.awaitTermination()


}}